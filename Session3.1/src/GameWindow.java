import controllers.PlaneController;
import utils.Utils;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

/**
 * Created by tungb on 7/31/2016.
 */
public class GameWindow extends Frame implements Runnable{
    Image background;
    Thread thread;
    BufferedImage bufferedImage;
    Graphics bufferedImageGraphics;
    PlaneController planeController;

    public GameWindow(){
        this.setVisible(true);
        this.setSize(600, 500);
        this.setLocation(0, 0);
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        this.setCursor(getToolkit().createCustomCursor(new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), null));
        this.bufferedImage = new BufferedImage(600, 500, BufferedImage.TYPE_INT_ARGB);
//        this.bufferedImageGraphics = bufferedImage.getGraphics();
        background = Utils.loadImage("resources/background.png");
        planeController = PlaneController.getPlaneController1();
        this.addKeyListener(planeController);
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void update(Graphics g) {
        this.bufferedImageGraphics = bufferedImage.getGraphics();
        bufferedImageGraphics.drawImage(background, 0, 0, null);
//        System.out.println("draw bg");
        planeController.draw(bufferedImageGraphics);
        g.drawImage(bufferedImage, 0, 0, null);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(17);
                planeController.run();
                repaint();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
