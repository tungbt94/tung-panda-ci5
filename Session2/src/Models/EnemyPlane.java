package Models;

/**
 * Created by tungb on 7/30/2016.
 */
public class EnemyPlane extends GameObject{
    public int hp;

    public EnemyPlane() {
        this(0, 0, 10);
    }

    public EnemyPlane(int x, int y, int hp) {
        this.x = x;
        this.y = y;
        this.hp = hp;
    }


}
