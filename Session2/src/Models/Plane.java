package Models;

import java.awt.*;

/**
 * Created by tungb on 7/27/2016.
 */
public class Plane extends  GameObject{
    public final static int SPEED = 10;
    public void move(Direction direction){
        switch (direction){
            case UP:
                y -= SPEED; break;
            case DOWN:
                y += SPEED; break;
            case LEFT:
                x -= SPEED; break;
            case RIGHT:
                x += SPEED; break;
        }
    }
}
