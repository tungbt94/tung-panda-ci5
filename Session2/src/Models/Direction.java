package Models;

/**
 * Created by tungb on 7/30/2016.
 */
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
