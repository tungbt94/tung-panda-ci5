package controllers;

import java.awt.*;

/**
 * Created by tungb on 7/31/2016.
 */
public interface BaseController {
    void draw(Graphics g);
    void run();
}
