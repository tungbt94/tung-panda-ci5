package controllers;

import models.GameObject;
import views.GameDrawer;

/**
 * Created by tungb on 7/31/2016.
 */
public class BulletController extends SingleController {
    private static final int SPEED = 20;
    public BulletController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dy = -SPEED;
    }

    @Override
    public void run() {
        super.run();
        if(gameObject.getY() < 0){
            gameObject.destroy();
        }
    }
}
