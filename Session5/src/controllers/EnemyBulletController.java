package controllers;

import models.GameObject;
import views.GameDrawer;

/**
 * Created by tungb on 8/10/2016.
 */
public class EnemyBulletController extends SingleController implements Colliable {
    public EnemyBulletController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
    }

    @Override
    public void onCollide(Colliable colliable) {
        if(colliable instanceof  PlaneController){
            colliable.getGameObject().destroy();
        }
    }
}
