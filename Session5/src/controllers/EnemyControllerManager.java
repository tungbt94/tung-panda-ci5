package controllers;

import models.Enemy;
import utils.Utils;
import views.ImageDrawer;

/**
 * Created by tungb on 8/10/2016.
 */
public class EnemyControllerManager extends ControllerManager {
    private int count;
    private static final int RESPAWN_BASE_ENEMY = 100;
    private static final int RESPAWN_THROUGH_ENEMY = 200;

    public static final EnemyControllerManager instance = new EnemyControllerManager();

    public EnemyControllerManager() {
        super();
        count = 0;
    }

    @Override
    public void run() {
        super.run();
        count++;
        int enemyX = 10;
        int enemyY = 10;
        if (count == RESPAWN_BASE_ENEMY) {
            spawnBaseEnemy(5, enemyX, enemyY, "plane1");
        }else if(count == RESPAWN_THROUGH_ENEMY){
            spawnThroughEnemy(5, enemyX, enemyY, "enemy_plane_white_3");
            count = 0;
        }
    }

    private void spawnBaseEnemy(int number, int enemyX, int enemyY, String name) {
        for (int i = 0; i < number; i++) {
            BaseEnemyController baseEnemyController = new BaseEnemyController(
                    new Enemy(enemyX + i * 100, enemyY),
                    new ImageDrawer("resources/" + name + ".png")
            );
            this.add(baseEnemyController);
        }
    }

    private void spawnThroughEnemy(int number, int enemyX, int enemyY, String name) {
        for (int i = 0; i < number; i++) {
            ThroughEnemyController throughEnemyController = new ThroughEnemyController(
                    new Enemy(enemyX + i * 100, enemyY, 2),
                    new ImageDrawer("resources/" + name + ".png")
            );
            this.add(throughEnemyController);
        }
    }

    public void removeAll(){
        singleControllerArrayList.clear();
    }

    public void removeNearPlane(){
        for(int i = 0; i < singleControllerArrayList.size(); i++){
            if(Utils.nearByPlane(singleControllerArrayList.get(i)))
                singleControllerArrayList.get(i).getGameObject().destroy();
        }
    }
}
