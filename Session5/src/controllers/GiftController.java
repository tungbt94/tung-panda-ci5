package controllers;

import models.GameObject;
import models.Gift;
import views.GameDrawer;

/**
 * Created by tungb on 8/10/2016.
 */
public class GiftController extends SingleController implements Colliable{
    private static int SPEED = 5;
    private String type;

    public GiftController(GameObject gameObject, GameDrawer gameDrawer, String type) {
        super(gameObject, gameDrawer);
        this.gameVector.dy = SPEED;
        this.type = type;
        CollisionPool.instance.add(this);
    }

    @Override
    public void onCollide(Colliable colliable) {
        if(colliable instanceof PlaneController){
            this.gameObject.destroy();
            action();
        }
    }

    private void action(){
        switch (type){
            case "bomb":
                EnemyControllerManager.instance.removeNearPlane();
                break;
            case "nuclear":
                EnemyControllerManager.instance.removeAll();
                break;
        }
    }

}
