package controllers;

import models.Gift;
import utils.CommonValues;
import views.ImageDrawer;

import java.util.Random;

/**
 * Created by tungb on 8/10/2016.
 */
public class GiftControllerManager extends ControllerManager {
    private int count;
    private static final int RESPAWN_BOMB = 250;
    private static final int RESPAWN_NUCLEAR = 500;
    private Random rand;

    public static final GiftControllerManager instance = new GiftControllerManager();

    public GiftControllerManager() {
        count = 0;
        rand = new Random();
    }

    @Override
    public void run() {
        super.run();
        count++;
        if(count == RESPAWN_BOMB){
            respawnGift("bomb");
        }else if (count == RESPAWN_NUCLEAR){
            respawnGift("nuclear");
            count = 0;
        }
    }

    private void respawnGift(String name){
        GiftController giftController = new GiftController(
                new Gift(rand.nextInt(CommonValues.SCREEN_WIDTH - Gift.WIDTH), 0),
                new ImageDrawer("resources/"+name+".png"),
                name
        );
        this.add(giftController);
    }
}
