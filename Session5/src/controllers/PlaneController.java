package controllers;

import models.Bullet;
import models.GameObject;
import models.Plane;
import utils.CommonValues;
import views.GameDrawer;
import views.ImageDrawer;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by tungb on 8/10/2016.
 */
public class PlaneController extends SingleController implements KeyListener, Colliable {
    public  static final int SPEED = 10;
    public  static final int ATK_SPEED = 3;
    private int count;

    private ControllerManager bulletManager;
    private GameInput gameInput;

    public static final PlaneController instance = new PlaneController(
            new Plane(250, 600),
            new ImageDrawer("resources/plane3.png")
    );

    public PlaneController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.bulletManager = new ControllerManager();
        this.gameInput = new GameInput();
        CollisionPool.instance.add(this);
    }

    @Override
    public void onCollide(Colliable colliable) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_UP :
                this.gameInput.keyUp = true;
                break;
            case KeyEvent.VK_DOWN :
                this.gameInput.keyDown = true;
                break;
            case KeyEvent.VK_RIGHT :
                this.gameInput.keyRight = true;
                break;
            case KeyEvent.VK_LEFT :
                this.gameInput.keyLeft = true;
                break;
            case KeyEvent.VK_SPACE :
                this.gameInput.keySpace = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_UP :
                this.gameInput.keyUp = false;
                break;
            case KeyEvent.VK_DOWN :
                this.gameInput.keyDown = false;
                break;
            case KeyEvent.VK_RIGHT :
                this.gameInput.keyRight = false;
                break;
            case KeyEvent.VK_LEFT :
                this.gameInput.keyLeft = false;
                break;
            case KeyEvent.VK_SPACE :
                this.gameInput.keySpace = false;
                break;
        }
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        bulletManager.draw(g);
    }

    @Override
    public void run() {
        count++;
        move();
        if(gameInput.keySpace && count > ATK_SPEED){
            BulletController bulletController = new BulletController(
                    new Bullet(this.gameObject.getMiddleX() - Bullet.WIDTH / 2, this.gameObject.getY()),
                    new ImageDrawer("resources/bullet.png")
            );
            bulletManager.add(bulletController);
            count = 0;
        }
        super.run();
        bulletManager.run();
    }

    public void move(){
        this.gameVector.dx = 0;
        this.gameVector.dy = 0;
        if(gameInput.keyDown && !gameInput.keyUp &&
                gameObject.getBottom() + SPEED <= CommonValues.SCREEN_HEIGHT)
            gameVector.dy = SPEED;
        else if(!gameInput.keyDown && gameInput.keyUp &&
                gameObject.getY() - SPEED >= 0)
            gameVector.dy = -SPEED;

        if(gameInput.keyRight && !gameInput.keyLeft &&
                gameObject.getRight() + SPEED <= CommonValues.SCREEN_WIDTH)
            gameVector.dx = SPEED;
        else if(!gameInput.keyRight && gameInput.keyLeft &&
                gameObject.getX() - SPEED >= 0)
            gameVector.dx = -SPEED;
    }
}
