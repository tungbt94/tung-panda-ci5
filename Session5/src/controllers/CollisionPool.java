package controllers;

import models.GameObject;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by tungb on 8/9/2016.
 */
public class CollisionPool implements BaseController{
    public static final CollisionPool instance = new CollisionPool();
    private ArrayList<Colliable> colliableArrayList;

    public CollisionPool() {
        colliableArrayList = new ArrayList<>();
    }

    public void add(Colliable colliable){
        this.colliableArrayList.add(colliable);
    }

    @Override
    public void draw(Graphics g) {
    }

    @Override
    public void run() {
        for(int i = 0; i < colliableArrayList.size() - 1; i++){
            for(int j = 0; j < colliableArrayList.size(); j++){
                checkOverlap(i, j);
            }
        }

        for(int i = 0; i < colliableArrayList.size() - 1; i++){
            if(!colliableArrayList.get(i).getGameObject().isAlive())
                colliableArrayList.remove(i);
        }
    }

    private void checkOverlap(int i, int j) {
        Colliable ci = colliableArrayList.get(i);
        Colliable cj = colliableArrayList.get(j);
        GameObject gi = ci.getGameObject();
        GameObject gj = cj.getGameObject();
        if(gi.overlap(gj)){
            ci.onCollide(cj);
            cj.onCollide(ci);
        }
    }
}
