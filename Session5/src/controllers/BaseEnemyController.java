package controllers;

import models.GameObject;
import views.GameDrawer;

/**
 * Created by tungb on 8/10/2016.
 */
public class BaseEnemyController extends SingleController implements Colliable{
    public static final int SPEED = 3;

    public BaseEnemyController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dy = SPEED;
        CollisionPool.instance.add(this);
    }

    @Override
    public void onCollide(Colliable colliable) {

    }
}
