package models;

import java.awt.*;

/**
 * Created by tungb on 8/9/2016.
 */
public abstract class GameObject {
    protected  int x;
    protected  int y;
    protected  int width;
    protected  int height;
    protected boolean isAlive;

    public GameObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        isAlive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public int getMiddleX(){
        return this.x + this.width / 2;
    }

    public int getBottom(){
        return this.y + this.height;
    }

    public int getRight(){
        return this.x + this.width;
    }

    public void move(GameVector gameVector){
        this.x += gameVector.dx;
        this.y += gameVector.dy;
    }

    private Rectangle getRectangle(){
        return new Rectangle(x, y, width, height);
    }

    public boolean overlap(GameObject gameObject){
        return this.getRectangle().intersects(gameObject.getRectangle());
    }

    public void destroy(){
        this.isAlive = false;
    }
}
