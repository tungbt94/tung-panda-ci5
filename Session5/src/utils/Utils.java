package utils;

import controllers.PlaneController;
import controllers.SingleController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by tungb on 8/9/2016.
 */
public class Utils {
    public static Image loadImage(String url){
        try {
            return ImageIO.read(new File(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean nearByPlane(SingleController enemy){
        int distanceX = Math.abs(PlaneController.instance.getGameObject().getX() - enemy.getGameObject().getX());
        int distanceY = Math.abs(PlaneController.instance.getGameObject().getY() - enemy.getGameObject().getY());
        if(Math.sqrt(distanceX * distanceX + distanceY * distanceY) < CommonValues.BOMB_RADIUS)
            return true;
        return false;
    }
}
