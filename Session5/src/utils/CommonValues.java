package utils;

/**
 * Created by tungb on 8/9/2016.
 */
public class CommonValues {
    public static final String NAME = "Plane";

    public static final int SCREEN_WIDTH = 600;
    public static final int SCREEN_HEIGHT = 800;

    public static final int BOMB_RADIUS = 200;
}
