import controllers.CollisionPool;
import controllers.EnemyManager;
import controllers.PlaneController;
import utils.Utils;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

/**
 * Created by tungb on 8/2/2016.
 */
public class GameWindow extends Frame implements Runnable{
    public static final int WIDTH = 600;
    public static final int HEIGHT = 800;
    Image background;
    BufferedImage bufferedImage;
    Graphics bufferedImageGraphics;

    Thread thread;

    public GameWindow(){
        this.setTitle("Plane");
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        this.addKeyListener(PlaneController.getInstance());
        background = Utils.loadImage("resources/background.png");
        this.bufferedImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        this.bufferedImageGraphics = bufferedImage.getGraphics();
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void update(Graphics g) {
        bufferedImageGraphics.drawImage(background,0,0, null);
        PlaneController.getInstance().draw(bufferedImageGraphics);
        EnemyManager.getInstance().draw(bufferedImageGraphics);
        g.drawImage(bufferedImage,0,0,null);
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(17);
                PlaneController.getInstance().run();
                EnemyManager.getInstance().run();
                CollisionPool.getInstance().run();
                repaint();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
