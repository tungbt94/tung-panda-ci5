package controllers;

import models.GameObject;
import models.GameVector;
import views.GameDrawer;

import java.awt.*;

/**
 * Created by tungb on 8/3/2016.
 */
public class SingleController implements BaseController {
    protected GameObject gameObject;
    protected GameVector gameVector;
    private GameDrawer gameDrawer;

    public SingleController(GameObject gameObject, GameDrawer gameDrawer) {
        this.gameObject = gameObject;
        this.gameDrawer = gameDrawer;
        gameVector = new GameVector();
    }

    public GameObject getGameObject() {
        return gameObject;
    }

    @Override
    public void draw(Graphics g) {
        gameDrawer.draw(g, gameObject);
    }

    @Override
    public void run() {
        gameObject.move(this.gameVector);
    }
}
