package controllers;

import models.Bullet;
import models.Direction;
import models.GameObject;
import models.Plane;
import views.GameDrawer;
import views.ImageDrawer;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by tungb on 8/3/2016.
 */
public class PlaneController extends SingleController implements KeyListener, Colliable {
    public static final int SPEED = 10;
    public static final int BULLET_SPEED = 20;

    private ControllerManager bulletManager;
    private GameInput gameInput;

    private static PlaneController instance;

    public static PlaneController getInstance() {
        if (instance == null) {
            instance = new PlaneController(new Plane(240, 600), new ImageDrawer("resources/plane4.png"));
        }
        return instance;
    }

    public PlaneController(Plane plane, GameDrawer gameDrawer) {
        super(plane, gameDrawer);
        this.bulletManager = new ControllerManager();
        this.gameInput = new GameInput();
        CollisionPool.getInstance().add(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                this.gameInput.keyUp = true;
                break;
            case KeyEvent.VK_DOWN:
                this.gameInput.keyDown = true;
                break;
            case KeyEvent.VK_LEFT:
                this.gameInput.keyLeft = true;
                break;
            case KeyEvent.VK_RIGHT:
                this.gameInput.keyRight = true;
                break;
            case KeyEvent.VK_SPACE:
                this.gameInput.keySpace = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                this.gameInput.keyUp = false;
                break;
            case KeyEvent.VK_DOWN:
                this.gameInput.keyDown = false;
                break;
            case KeyEvent.VK_LEFT:
                this.gameInput.keyLeft = false;
                break;
            case KeyEvent.VK_RIGHT:
                this.gameInput.keyRight = false;
                break;
            case KeyEvent.VK_SPACE:
                this.gameInput.keySpace = false;
                break;
        }
    }

    @Override
    public void onCollide(Colliable colliable) {

    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        bulletManager.draw(g);
    }

    @Override
    public void run() {
        this.gameVector.dx = 0;
        this.gameVector.dy = 0;
        if (gameInput.keyDown && !gameInput.keyUp)
            this.gameVector.dy = SPEED;
        else if (!gameInput.keyDown && gameInput.keyUp)
            this.gameVector.dy = -SPEED;
        if (gameInput.keyLeft && !gameInput.keyRight)
            this.gameVector.dx = -SPEED;
        else if (!gameInput.keyLeft && gameInput.keyRight)
            this.gameVector.dx = SPEED;
        if (gameInput.keySpace) {
            BulletController bulletController = new BulletController(
                    new Bullet(this.gameObject.middleX() - Bullet.WIDTH / 2, this.gameObject.getY() - Bullet.HEIGHT),
                    new ImageDrawer("resources/bullet.png"),
                    Direction.UP,
                    BULLET_SPEED
            );
            bulletManager.add(bulletController);
        }
        super.run();
        bulletManager.run();
    }
}
