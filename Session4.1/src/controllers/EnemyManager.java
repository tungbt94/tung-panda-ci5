package controllers;

import models.Bullet;
import models.Direction;
import models.Enemy;
import models.GameObject;
import views.ImageDrawer;

import java.awt.*;
import java.util.Random;

/**
 * Created by tungb on 8/3/2016.
 */
public class EnemyManager extends ControllerManager{
    private int time;
    private Random rand;
    private ControllerManager bulletManager;
    private static EnemyManager instance;
    private static final int BULLET_SPEED = 5;

    public static EnemyManager getInstance() {
        if(instance == null){
            instance = new EnemyManager();
        }
        return instance;
    }

    private EnemyManager() {
        super();
        time = 0;
        rand = new Random();
        bulletManager = new ControllerManager();
        addEnemy(10, 10, 5,Direction.DOWN);

    }

    @Override
    public void run() {
        super.run();
        time++;
        if(time == Integer.MAX_VALUE)
            time = 0;
        if (time % 50 == 0 ) {
            shot();
        }
        if(time % 100 == 0){
            addEnemy(20, 10, 5,Direction.DOWN);
        }else if(time % 150 == 0){
            addEnemy(20, 80, 5,Direction.RIGHT);
        }
        bulletManager.run();
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        bulletManager.draw(g);
    }

    private void addEnemy(int enemyX, int enemyY, int numberEnemy, Direction direction){
        int spaceX = 100;
        String enemyName = "plane1";
        switch (direction) {
            case DOWN:
                spaceX = 100;
                enemyName = "plane1";
                break;
            case LEFT:
                spaceX = 100;
                enemyName = "enemy_plane_white_1";
                break;
            case RIGHT:
                spaceX = -100;
                enemyName = "enemy_plane_yellow_3";
                break;
        }
        for(int i = 0; i < numberEnemy; i++){
            EnemyController enemyController = new EnemyController(
                    new Enemy(enemyX = enemyX + spaceX, enemyY),
                    new ImageDrawer("resources/"+enemyName+".png"),
                    direction
            );
            this.add(enemyController);

        }
    }

    private void shot(){
        for (int i = 0; i < this.getSingleControllersArray().size(); i++) {
            GameObject enemy = this.getSingleControllersArray().get(i).getGameObject();
            if (enemy.getY() < PlaneController.getInstance().getGameObject().getY() - Bullet.HEIGHT) {
                BulletController bulletController = new BulletController(
                        new Bullet(enemy.middleX(), enemy.getY() + enemy.getHeight()),
                        new ImageDrawer("resources/enemy_bullet.png"),
                        Direction.DOWN,
                        BULLET_SPEED
                );
                bulletManager.add(bulletController);
            }
        }
    }
}
