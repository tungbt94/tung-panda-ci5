package controllers;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by tungb on 8/3/2016.
 */
public class ControllerManager implements BaseController{
    private ArrayList<SingleController> singleControllersArray;

    public ControllerManager() {
        singleControllersArray = new ArrayList<>();
    }

    public void add(SingleController singleController){
        this.singleControllersArray.add(singleController);
    }

    public ArrayList<SingleController> getSingleControllersArray() {
        return singleControllersArray;
    }

    @Override
    public void draw(Graphics g) {
        for(int i = 0; i < singleControllersArray.size(); i++)
            singleControllersArray.get(i).draw(g);
    }

    @Override
    public void run() {
        for(int i = 0; i < singleControllersArray.size(); i++)
            if(!singleControllersArray.get(i).getGameObject().isAlive())
                singleControllersArray.remove(i);
            else
                singleControllersArray.get(i).run();
    }
}
