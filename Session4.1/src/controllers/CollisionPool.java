package controllers;

import models.GameObject;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by tungb on 8/3/2016.
 */
public class CollisionPool implements BaseController {
    private ArrayList<Colliable> colliableArrayList;
    private static CollisionPool instance;

    public static CollisionPool getInstance() {
        if(instance==null){
            instance = new CollisionPool();
        }
        return instance;
    }

    public CollisionPool() {
        colliableArrayList = new ArrayList<>();
    }

    public void add(Colliable colliable){
        colliableArrayList.add(colliable);
    }

    @Override
    public void draw(Graphics g) {
    }

    @Override
    public void run() {
        for(int i = 0; i < colliableArrayList.size() - 1; i++){
            for(int j = i + 1; j < colliableArrayList.size(); j++){
                Colliable ci = colliableArrayList.get(i);
                Colliable cj = colliableArrayList.get(j);
                GameObject gi = ci.getGameObject();
                GameObject gj = cj.getGameObject();
                if(gi.overlap(gj)){
                    ci.onCollide(cj);
                    cj.onCollide(ci);
                }
            }
        }
        for(int i = 0; i < colliableArrayList.size(); i++){
            if(!colliableArrayList.get(i).getGameObject().isAlive())
                colliableArrayList.remove(i);
        }
    }
}
