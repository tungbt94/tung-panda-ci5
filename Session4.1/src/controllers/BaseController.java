package controllers;

import java.awt.*;

/**
 * Created by tungb on 8/3/2016.
 */
public interface BaseController {
    public void draw(Graphics g);
    public void run();
}
