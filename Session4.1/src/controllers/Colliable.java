package controllers;

import models.GameObject;

/**
 * Created by tungb on 8/3/2016.
 */
public interface Colliable {
    public GameObject getGameObject();

    public void onCollide(Colliable colliable);
}
