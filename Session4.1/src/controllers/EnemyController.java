package controllers;

import models.Bullet;
import models.Direction;
import models.GameObject;
import views.GameDrawer;
import views.ImageDrawer;

import java.awt.*;

/**
 * Created by tungb on 8/3/2016.
 */
public class EnemyController extends SingleController implements Colliable {
    private static final int SPEED = 3;

    public EnemyController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dy = SPEED;
        CollisionPool.getInstance().add(this);
    }

    public EnemyController(GameObject gameObject, GameDrawer gameDrawer, Direction direction) {
        super(gameObject, gameDrawer);
        switch (direction) {
            case DOWN:
                this.gameVector.dy = SPEED;
                break;
            case LEFT:
                this.gameVector.dx = -SPEED;
                break;
            case RIGHT:
                this.gameVector.dx = SPEED;
                break;
        }
        CollisionPool.getInstance().add(this);
    }

    @Override
    public void onCollide(Colliable colliable) {

    }

    @Override
    public void run() {
        super.run();
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
    }
}
