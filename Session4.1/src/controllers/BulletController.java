package controllers;

import models.Direction;
import models.Enemy;
import models.GameObject;
import views.GameDrawer;

/**
 * Created by tungb on 8/3/2016.
 */
public class BulletController extends SingleController implements Colliable {
//    private static final int SPEED = 20;
    private Direction direction;

    public BulletController(GameObject gameObject, GameDrawer gameDrawer, Direction direction, int speed) {
        super(gameObject, gameDrawer);
        this.direction = direction;
        switch (direction){
            case UP:
                this.gameVector.dy = -speed;
                break;
            case DOWN:
                this.gameVector.dy = speed;
                break;
        }
        CollisionPool.getInstance().add(this);
    }

    @Override
    public void run() {
        super.run();
        if(gameObject.getY() < 0 || gameObject.getY() > 800)
            gameObject.destroy();
    }

    @Override
    public void onCollide(Colliable colliable) {
        if(colliable instanceof EnemyController && this.direction == Direction.UP){
            colliable.getGameObject().destroy();
            this.getGameObject().destroy();
        }else if(colliable instanceof PlaneController && this.direction == Direction.DOWN){
            System.out.println("tru hp plane");
            this.getGameObject().destroy();
        }
    }
}
