package models;

/**
 * Created by tungb on 8/3/2016.
 */
public enum Direction {
    UP,
    DOWN,
    RIGHT,
    LEFT
}
