package models;

/**
 * Created by tungb on 8/3/2016.
 */
public class Plane extends GameObject{
    public static final int WIDTH = 70;
    public static final int HEIGHT = 50;
    public Plane(int x, int y, int width, int height) {
        super(x, y, WIDTH, HEIGHT);
    }
    public Plane(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
    }
}
