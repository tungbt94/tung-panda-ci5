package models;

/**
 * Created by tungb on 8/3/2016.
 */
public class Bullet extends GameObject {
    public static final int WIDTH = 13;
    public static final int HEIGHT = 30;
    public Bullet(int x, int y, int width, int height) {
        super(x, y, WIDTH, HEIGHT);
    }
    public Bullet(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
    }
}
