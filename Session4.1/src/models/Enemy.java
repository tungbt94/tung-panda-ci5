package models;

/**
 * Created by tungb on 8/3/2016.
 */
public class Enemy extends GameObject{
    public static final int DEFAULT_IDTH = 35;
    public static final int DEFAULT_HEIGHT = 30;
    public Enemy(int x, int y, int width, int height) {
        super(x, y, DEFAULT_IDTH, DEFAULT_HEIGHT);
    }
    public Enemy(int x, int y) {
        super(x, y, DEFAULT_IDTH, DEFAULT_HEIGHT);
    }
}
