package views;

import models.GameObject;

import java.awt.*;

/**
 * Created by tungb on 8/3/2016.
 */
public interface GameDrawer {
    void draw(Graphics g, GameObject gameObject);
}
