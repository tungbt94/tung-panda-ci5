package utils;

import controllers.PlaneController;
import controllers.SingleController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by tungb on 8/9/2016.
 */
public class Utils {
    public static Image loadImage(String url){
        try {
            return ImageIO.read(new File(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
