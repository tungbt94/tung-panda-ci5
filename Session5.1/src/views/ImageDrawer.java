package views;

import models.GameObject;
import utils.Utils;

import java.awt.*;

/**
 * Created by tungb on 8/9/2016.
 */
public class ImageDrawer implements  GameDrawer{
    private Image image;

    public ImageDrawer(String url) {
        this.image = Utils.loadImage(url);
    }

    @Override
    public void draw(Graphics g, GameObject gameObject) {
        g.drawImage(image, gameObject.getX(), gameObject.getY(), gameObject.getWidth(), gameObject.getHeight(), null);
    }
}
