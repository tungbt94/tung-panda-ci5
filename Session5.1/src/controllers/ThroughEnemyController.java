package controllers;

import models.EnemyBullet;
import models.GameObject;
import models.GameVector;
import utils.CommonValues;
import views.GameDrawer;
import views.ImageDrawer;

import java.awt.*;

/**
 * Created by tungb on 8/10/2016.
 */
public class ThroughEnemyController extends BaseEnemyController {
    private static final int MOVEMENT_SPEED = 1;
    private static final int SHOT_SPEED = 150;
    private static final int BULLET_SPEED = 3;
    private int count;

    public ThroughEnemyController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dx = MOVEMENT_SPEED;
        this.gameVector.dy = MOVEMENT_SPEED;
        count = 0;
    }

    @Override
    public void run() {
        super.run();
        count++;
        if (count == SHOT_SPEED) {
            shot();
        }
        if (this.gameObject.getRight() >= CommonValues.SCREEN_WIDTH || this.gameObject.getX() <= 0)
            this.gameVector.dx = -this.gameVector.dx;
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
    }

    private void shot(){
        EnemyBulletController enemyBulletController = new EnemyBulletController(
                new EnemyBullet(this.gameObject.getMiddleX() - EnemyBullet.SIZE / 2, this.gameObject.getBottom()),
                new ImageDrawer("resources/enemy_bullet.png")
        );
        int dy = PlaneController.instance.getGameObject().getY() - this.gameObject.getY();
        if (dy > 0) {
            int dx = PlaneController.instance.getGameObject().getX() - this.gameObject.getX();
            double ratio = Math.sqrt(dx * dx + dy * dy) / BULLET_SPEED;
            enemyBulletController.getGameVector().dx = (int) (dx / ratio);
            enemyBulletController.getGameVector().dy = (int) (dy / ratio);
            EnemyBulletControllerManager.instance.add(enemyBulletController);
        }
    }
}
