package controllers;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by tungb on 8/9/2016.
 */
public class ControllerManager implements BaseController{
    protected ArrayList<SingleController> singleControllerArrayList;

    public ControllerManager() {
        singleControllerArrayList = new ArrayList<>();
    }
    public void add(SingleController singleController){
        this.singleControllerArrayList.add(singleController);
    }

    public ArrayList<SingleController> getSingleControllerArrayList() {
        return singleControllerArrayList;
    }

    @Override
    public void draw(Graphics g) {
        for (int i = 0; i < singleControllerArrayList.size(); i++)
            if(singleControllerArrayList.get(i).getGameObject().isAlive())
                singleControllerArrayList.get(i).draw(g);
    }

    @Override
    public void run() {
        for (int i = 0; i < singleControllerArrayList.size(); i++)
            if(!singleControllerArrayList.get(i).getGameObject().isAlive())
                singleControllerArrayList.remove(i);
            else
                singleControllerArrayList.get(i).run();
    }
}
