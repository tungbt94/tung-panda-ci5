package controllers;

import java.awt.*;

/**
 * Created by tungb on 8/9/2016.
 */
public interface BaseController {
    void draw(Graphics g);
    void run();
}
