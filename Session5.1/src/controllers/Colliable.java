package controllers;

import models.GameObject;

/**
 * Created by tungb on 8/9/2016.
 */
public interface Colliable {
    GameObject getGameObject();
    void onCollide(Colliable colliable);
}
