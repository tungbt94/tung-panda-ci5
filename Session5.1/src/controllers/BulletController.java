package controllers;

import models.Bullet;
import models.GameObject;
import models.GameObjectWithHP;
import views.GameDrawer;

/**
 * Created by tungb on 8/10/2016.
 */
public class BulletController extends SingleController implements Colliable {
    private static final int SPEED = 20;

    public BulletController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dy = - SPEED;
        CollisionPool.instance.add(this);
    }

    @Override
    public void run() {
        super.run();
        if (gameObject.getY() < 0)
           gameObject.destroy();
    }

    @Override
    public void onCollide(Colliable colliable) {
        if (colliable instanceof BaseEnemyController){
            Bullet bullet = (Bullet) gameObject;
            ((GameObjectWithHP)colliable.getGameObject()).decreaseHP(bullet.getDamage());
            this.gameObject.destroy();
        }
    }
}
