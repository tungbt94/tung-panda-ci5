package models;

/**
 * Created by tungb on 8/9/2016.
 */
public class Bullet extends  GameObject{
    public static final int WIDTH = 13;
    public static final int HEIGHT = 30;
    private int damage;

    public Bullet(int x, int y, int damage) {
        super(x, y, WIDTH, HEIGHT);
        this.damage = damage;
    }

    public Bullet(int x, int y) {
        this(x, y, 2);
    }

    public int getDamage() {
        return damage;
    }
}
