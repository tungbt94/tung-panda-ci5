package models;

/**
 * Created by tungb on 8/9/2016.
 */
public class Enemy extends GameObjectWithHP {
    public static final int DEFAULT_WIDTH = 45;
    public static final int DEFAULT_HEIGHT = 30;

    public Enemy(int x, int y, int width, int height, int hp, int maxHP) {
        super(x, y, width, height, hp, maxHP);
    }

    public Enemy(int x, int y, int maxHP) {
        this(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, maxHP, maxHP);
    }

    public Enemy(int x, int y) {
        this(x, y, 1);
    }
}
