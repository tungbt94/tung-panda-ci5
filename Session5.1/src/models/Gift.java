package models;

/**
 * Created by tungb on 8/10/2016.
 */
public class Gift extends GameObject {
    public static final int WIDTH = 40;
    public static final int HEIGHT = 40;

    public Gift(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
    }

    public Gift(int x, int y, String type) {
        super(x, y, WIDTH, HEIGHT);
    }
}
