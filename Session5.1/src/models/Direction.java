package models;

/**
 * Created by tungb on 8/9/2016.
 */
public enum Direction {
    UP,
    RIGHT,
    LEFT,
    DOWN
}
