import controllers.*;
import models.Plane;
import utils.CommonValues;
import utils.Utils;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

/**
 * Created by tungb on 8/9/2016.
 */
public class GameWindow extends Frame implements Runnable{
    Image background;

    BufferedImage bufferedImage;
    Graphics bufferedImageGraphics;

    Thread thread;

    public GameWindow(){
        this.setTitle(CommonValues.NAME);
        this.setVisible(true);
        this.setSize(CommonValues.SCREEN_WIDTH, CommonValues.SCREEN_HEIGHT);
        this.setLocationRelativeTo(null);
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        this.addKeyListener(PlaneController.instance);
        background = Utils.loadImage("resources/background.png");
        this.bufferedImage = new BufferedImage(CommonValues.SCREEN_WIDTH, CommonValues.SCREEN_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        this.bufferedImageGraphics = bufferedImage.getGraphics();

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void update(Graphics g) {
//        super.update(g);
        bufferedImageGraphics.drawImage(background, 0, 0, null);
        PlaneController.instance.draw(bufferedImageGraphics);
        EnemyControllerManager.instance.draw(bufferedImageGraphics);
        EnemyBulletControllerManager.instance.draw(bufferedImageGraphics);
        GiftControllerManager.instance.draw(bufferedImageGraphics);
        g.drawImage(bufferedImage, 0, 0, null);
    }

    @Override
    public void run() {
        while (true){
            try {
                PlaneController.instance.run();
                EnemyControllerManager.instance.run();
                EnemyBulletControllerManager.instance.run();
                GiftControllerManager.instance.run();
                CollisionPool.instance.run();
                Thread.sleep(17);
                repaint();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ;
        }
    }
}
